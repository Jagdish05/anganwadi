package com.example.jagdish.anganwadi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class Navigation extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private FirebaseAuth auth;
    private  FirebaseDatabase database;

    private DatabaseReference mDatabase;
    private  DatabaseReference mDatabase_current_user;
    private  DatabaseReference myRef_current_user;
    private DatabaseReference myRef;

    NavigationView navigationView;
    //int count=1;
    //long total_children;
    String TAG="Navigation";
    //ProgressBar progressBar;

    private ProgressDialog progress;
    CircularProgressView progressView;

    String string_for_textView_username,string_for_textView_user_designation,string_for_imageView_user_image;

    TextView textView_username_show;
    TextView textView_user_degination;
    ImageView  imageView_user_profile;

    FrameLayout frameLayout1,frameLayout2;
    Fragment fragment = null;
    private String userID=null;
    private boolean doubleBackToExitPressedOnce = false;
    FloatingActionButton fab;
    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
       // Log.e(TAG," NAvigation : 0");

        //FirebaseCrash.report(new Exception(this.getClass().getSimpleName()));
        //FirebaseCrash.log("Activity created");

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(0).setChecked(true);
        Log.e(TAG," NAvigation : 1");

        navigationView.getMenu().performIdentifierAction(R.id.Overview, 0);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        //frameLayout1 = (FrameLayout)findViewById(R.id.nav_frameLayout1);
        //frameLayout2 = (FrameLayout)findViewById(R.id.nav_frameLayout2);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, new OverViewAllData());
        ft.commit();
        Log.e(TAG," NAvigation : 1");

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
               // startActivity(new Intent(Navigation.this,Add_user.class));
                navigationView.getMenu().getItem(2).setChecked(true);
                FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.content_frame, new Add_user());
                tx.commit();

            }
        });



        //progressBar =(ProgressBar)findViewById(R.id.progressBar);
//        progress=new ProgressDialog(this);
//       // progress.setMessage("Downloading Music");
//        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        //progress.setIndeterminate(true);
//        //progress.setProgress(0);
//        progress.(false);
        //progress.show();
        //toggle.setDrawerIndicatorEnabled(false);
        //fab.setVisibility(View.INVISIBLE);
        progressView = (CircularProgressView) findViewById(R.id.progress_view);
        progressView.startAnimation();

        auth = FirebaseAuth.getInstance();
        userID = auth.getCurrentUser().getUid();
        database = FirebaseDatabase.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference("Profile");
        mDatabase_current_user = FirebaseDatabase.getInstance().getReference(userID);
        myRef_current_user = database.getReference(auth.getCurrentUser().getUid());
        myRef = database.getReference();

//        if (database!=database){
//            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
//        }
        Log.e(TAG," NAvigation : 2");
       // progressBar.isShown();
        mDatabase.keepSynced(true);
        Query query = mDatabase.child(userID);
        //Log.e(TAG,"Query:");
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //Log.e(TAG,"Query: 1 "+dataSnapshot);
                if (dataSnapshot.exists()) {
                    //Log.e(TAG,"Query: 2 "+dataSnapshot);
                    //startActivity(new Intent(Navigation.this,ProfileView.class));
                    //finish();
                    //fragment = new ProfileView();
                    string_for_textView_username = (String) dataSnapshot.child("username").getValue();
                    string_for_textView_user_designation = (String) dataSnapshot.child("designation").getValue();
                    string_for_imageView_user_image = (String) dataSnapshot.child("image_pic").getValue();

//                    Log.e(TAG,"Found A User is "+string_for_textView_username);
//                    Log.e(TAG,"Found A User is "+string_for_imageView_user_image);
//                    Log.e(TAG,"Found A User is "+string_for_textView_user_designation);
                    update_profile_data();

//
                }else {
                    //Log.e(TAG,"Query: 3 "+dataSnapshot);
                    //fragment = new Profile();
                    //startActivity(new Intent(Navigation.this,Profile.class));
                    //finish();
                }
                progressView.stopAnimation();
                progressView.setVisibility(View.GONE);
//                fab.setVisibility(View.VISIBLE);
//                toggle.setDrawerIndicatorEnabled(true);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


//        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                //Log.e(TAG,"Profile  Total Child : "+dataSnapshot.getChildrenCount());
//                //Log.e(TAG,"Profile  4 : "+dataSnapshot.getChildren());
//                //total_children = dataSnapshot.getChildrenCount();
//                progressView.stopAnimation();
//                progressView.setVisibility(View.GONE);
//
////                FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
////                tx.replace(R.id.content_frame, new OverViewAllData());
////                tx.commit();
//
//                //frameLayout1.setVisibility(View.VISIBLE);
//                //frameLayout2.setVisibility(View.VISIBLE);
//                Log.e(TAG,"Found A User is "+dataSnapshot);
//
//                for (DataSnapshot snapshot:dataSnapshot.getChildren()){
//                    if (snapshot.getKey().equals(userID)){
//
//                        //DataSnapshot snap = snapshot;
//
//                        //Log.e(TAG,"Found A User is "+snapshot);
//                        //Log.e(TAG,"Found A User is "+snap);
//
//                        string_for_textView_username = (String) snapshot.child("username").getValue();
//                        string_for_textView_user_designation = (String) snapshot.child("designation").getValue();
//                        string_for_imageView_user_image = (String) snapshot.child("image_pic").getValue();
//
//                        Log.e(TAG,"Found A User is "+string_for_textView_username);
//                        //Log.e(TAG,"Found A User is "+string_for_imageView_user_image);
//                        Log.e(TAG,"Found A User is "+string_for_textView_user_designation);
//
//                        update_profile_data();
//                    }
//                }
//            }
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });

        if (auth.getCurrentUser() != null) {
           // Toast.makeText(Navigation.this,"Get Current User :"+auth.getCurrentUser().getProviderId(),Toast.LENGTH_LONG).show();
           // Toast.makeText(Navigation.this,"Get Current User :"+auth.getCurrentUser().getUid(),Toast.LENGTH_LONG).show();

           // userID = auth.getCurrentUser().getUid();
        }



        navigationView.setNavigationItemSelectedListener(this);
        View header=navigationView.getHeaderView(0);

         textView_username_show = (TextView)header.findViewById(R.id.textView_username_show);
         textView_user_degination = (TextView)header.findViewById(R.id.textView_user_degination);
          imageView_user_profile= (ImageView) header.findViewById(R.id.imageView_user_profile);

        //textView_username_show.setText(string_for_textView_username);
       // Log.e(TAG," Username 1"+string_for_textView_username);
        //Log.e(TAG," User Designation 2"+string_for_textView_user_designation);


    }
    public void update_profile_data(){

        //Log.e(TAG," Username "+string_for_textView_username);
       // Log.e(TAG," User Designation "+string_for_textView_user_designation);

        try {
            textView_user_degination.setText(string_for_textView_user_designation);
            textView_username_show.setText(string_for_textView_username);
            byte[] imageAsBytes = Base64.decode(string_for_imageView_user_image.getBytes(), Base64.DEFAULT);
            imageView_user_profile.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        this.doubleBackToExitPressedOnce = false;

    }
    private static final int TIME_INTERVAL = 2000; // # milliseconds, desired time passed between two back presses.
    private long mBackPressed;
    @Override
    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }

        if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis())
        {
            super.onBackPressed();
            return;
        }
        else {
            //Toast.makeText(getBaseContext(), "Tap back button in order to exit", Toast.LENGTH_SHORT).show();
        }

        mBackPressed = System.currentTimeMillis();
//        startActivity(new Intent(getApplication(),Navigation.class));
        //Toast.makeText(getApplication(), "Enter valid email id ", Toast.LENGTH_LONG).show();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, new OverViewAllData());
        ft.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
         if (id == R.id.action_logout){
            startActivity(new Intent(Navigation.this, Logout.class));
            finish();
     return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.Overview) {
            //startActivity(new Intent(Navigation.this,Add_user.class));
            //finish();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, new OverViewAllData());
            ft.addToBackStack("TAG");
            ft.commit();
        } else if (id == R.id.profile) {

            //count = 1 ;
            // Handle the camera action

//            startActivity(new Intent(Navigation.this,ProfileView.class));
//            finish();

            //final String userID = auth.getCurrentUser().getUid();
//           // byte[] decodeValue = Base64.decode(userID, Base64.DEFAULT);
            //Log.e(TAG,"Query UserID :" +userID);
            Query query = mDatabase.child(userID);
            //Log.e(TAG,"Query:");
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                   // Log.e(TAG,"Query: 1 "+dataSnapshot);
                    if (dataSnapshot.exists()) {
                        //Log.e(TAG,"Query: 2 "+dataSnapshot);
                        //startActivity(new Intent(Navigation.this,ProfileView.class));
                        //finish();
                        //fragment = new ProfileView();
                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.content_frame, new ProfileView());
//                        ft.addToBackStack(null);
                        ft.commit();
                    }else {
                        //Log.e(TAG,"Query: 3 "+dataSnapshot);
                        //fragment = new Profile();
                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.content_frame, new Profile());
//                        ft.addToBackStack(null);
                        ft.commit();
                        //startActivity(new Intent(Navigation.this,Profile.class));
                        //finish();
                    }
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else if (id == R.id.set_user_data) {
            //startActivity(new Intent(Navigation.this,Add_user.class));
            //finish();
            //fragment = new Add_user();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, new Add_user());
//            ft.addToBackStack(null);
            ft.commit();
        } else if (id == R.id.show_all_student_data) {
            //fragment = new RecyclerviewStudentDataShow();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, new RecyclerviewStudentDataShow());
//            ft.addToBackStack(null);
            ft.commit();
        }else if (id == R.id.add_anganwadi_data){
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, new Add_Anganwadi_data());
//            ft.addToBackStack(null);
            ft.commit();
        }else if (id == R.id.show_all_anganwadi_data){
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, new RecycleviewAnganwadiDataShow());
//            ft.addToBackStack(null);
            ft.commit();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
