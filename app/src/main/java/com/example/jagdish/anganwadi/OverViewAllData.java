package com.example.jagdish.anganwadi;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Utils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static com.example.jagdish.anganwadi.R.id.view_Normal1;

/**
 * Created by jagdish on 17/05/17.
 */

public class OverViewAllData extends Fragment{

    private DatabaseReference myRef_current_user;
    private FirebaseAuth auth;
    private  FirebaseDatabase database;

    //    TextView text_Obese_Class_III,text_Obese_Class_I,text_Obese_Class_II,text_Severely_Underweight,text_Overweight,text_Underweight,text_Normal;
    ViewGroup root;
    int Normal = 0;
    int Overweight = 0;
    int Underweight = 0;
    int Severely_Underweight = 0;
    int Obese_Class_III = 0;
    int Obese_Class_II = 0;
    int Obese_Class_I = 0;
    int other = 0;

    int total_count = 0;
    ArrayList<Entry> BARENTRY ;
    ArrayList<String> BarEntryLabels ;
    PieDataSet Bardataset ;
    PieData BARDATA ;
    PieChart chart;
    Legend legend;
    String TAG="OverViewAllData";
    int i = -1;
    int j=0;
    Entry ee;
    SharedPreferences.Editor editor;

    LinearLayout childLayout;
    TableLayout table;

    View view_normal,view_Severely_Overweight,view_Moderately_Obese,view_Overweight,view_Severely_Underweight,
            view_Underweight,view_Severelyy_Obese;
    TextView view_normal1,view_Severely_Overweight1,view_Moderately_Obese1,view_Overweight1,view_Severely_Underweight1,
            view_Underweight1,view_Severelyy_Obese1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        root = (ViewGroup)inflater.inflate(R.layout.over_view_all_data,null);
        FirebaseCrash.report(new Exception(getActivity().getClass().getSimpleName()));
        FirebaseCrash.log("Activity created");

        chart = (PieChart) root.findViewById(R.id.chart);
        //childLayout =(LinearLayout) root.findViewById(R.id.child_layout);
        legend = chart.getLegend();
        table = (TableLayout) root.findViewById(R.id.TableLayout01);


        view_normal = (View)root.findViewById(R.id.view_Normal);
        view_Moderately_Obese = (View)root.findViewById(R.id.view_Moderately_Obese);
        view_Severely_Overweight = (View)root.findViewById(R.id.view_Severely_Overweight);
        view_Overweight = (View)root.findViewById(R.id.view_Overweight);
        view_Severely_Underweight = (View)root.findViewById(R.id.view_Severely_Underweight);
        view_Underweight = (View)root.findViewById(R.id.view_Underweight);
        view_Severelyy_Obese = (View)root.findViewById(R.id.view_Severelyy_Obese);

        view_normal1 = (TextView) root.findViewById(view_Normal1);
        view_Moderately_Obese1 = (TextView)root.findViewById(R.id.view_Moderately_Obese1);
        view_Severely_Overweight1 = (TextView)root.findViewById(R.id.view_Severely_Overweight1);
        view_Overweight1 = (TextView)root.findViewById(R.id.view_Overweight1);
        view_Severely_Underweight1 = (TextView)root.findViewById(R.id.view_Severely_Underweight1);
        view_Severelyy_Obese1 = (TextView)root.findViewById(R.id.view_Severelyy_Obese1);
        view_Underweight1 = (TextView) root.findViewById(R.id.view_Underweight1);


        //chart.setDrawGridBackground(false);

        //chart.setDrawBarShadow(false);
        editor =  getActivity().getSharedPreferences("For_hint_swipe", Context.MODE_PRIVATE).edit();

        BARENTRY = new ArrayList<>();

        auth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();

        myRef_current_user = database.getReference(auth.getCurrentUser().getUid());
        myRef_current_user.keepSynced(true);

        //Log.e(TAG," OverView All DAta : 0");

        myRef_current_user.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //Log.e(TAG," OverView All DAta : 1");

                //Log.e(TAG,dataSnapshot.getKey()+" currunt User 12: "+dataSnapshot + "");

                for (DataSnapshot snap: dataSnapshot.getChildren()) {
                    //Log.e(TAG,snap.getKey()+" currunt User : "+snap.getChildrenCount() + "");
                    //Log.e(TAG,snap.getKey()+" currunt User : "+snap.child("bmi_status").getValue() + "");

                    String check= (String) snap.child("bmi_status").getValue();
                    if (check.contains("Normal")){
                        Normal++;
                    }else if (check.contains("Overweight")){
                        Overweight++;
                    }else if (check.contains("Underweight")){
                        Underweight++;
                    }else if (check.contains("Severely underweight")){
                        Severely_Underweight++;
                    }else if (check.contains("Obese Class III")){
                        Obese_Class_III++;
                    }else if (check.contains("Obese Class II")){
                        Obese_Class_II++;
                    }else if (check.contains("Obese Class I")){
                        Obese_Class_I++;
                    }else {
                        other++;
                    }
                }
//                Log.e(TAG,"Normal :"+Normal + " \nOverweight :"+Overweight+"  \nUnderweight :"+Underweight+"" +
//                        " \nSeverely_Underweight :"+Severely_Underweight+" \nObese_Class_I :"+Obese_Class_I+"" +
//                        " \nObese_Class_II :"+Obese_Class_II+" \nObese_Class_III :"+Obese_Class_III+" Other : "+other);
//                text_Normal.setText(""+Normal);
//                text_Overweight.setText(""+Overweight);
//                text_Underweight.setText(""+Underweight);
//                text_Severely_Underweight.setText(""+Severely_Underweight);
//                text_Obese_Class_I.setText(""+Obese_Class_I);
//                text_Obese_Class_II.setText(""+Obese_Class_II);
//                text_Obese_Class_III.setText(""+Obese_Class_III);



//                BARENTRY.add(new BarEntry(Normal, 0));
//                BARENTRY.add(new BarEntry(Overweight, 1));
//                BARENTRY.add(new BarEntry(Underweight, 2));
//                BARENTRY.add(new BarEntry(Severely_Underweight, 3));
//                BARENTRY.add(new BarEntry(Obese_Class_I, 4));
//                BARENTRY.add(new BarEntry(Obese_Class_II, 5));
//                BARENTRY.add(new BarEntry(Obese_Class_III, 6));

                BarEntryLabels = new ArrayList<String>();
                //chart.getXAxis().setTextColor(getResources().getColor(R.color.colorPrimaryDark));

                if (Normal != 0){
                    //BarEntryLabels.add("Normal");
                    total_count = total_count+1;
                }
                if (Overweight != 0) {
                    //BarEntryLabels.add("Overweight");
                    total_count = total_count+1;
                }

                if (Underweight != 0) {
                    //BarEntryLabels.add("Underweight");
                    total_count = total_count+1;

                }
                if (Severely_Underweight != 0) {
                    //BarEntryLabels.add("Severely Underweight");
                    total_count = total_count+1;

                }
                if (Obese_Class_I != 0) {
                    //BarEntryLabels.add("Moderately Obese");
                    total_count = total_count+1;
                }
                if (Obese_Class_II != 0) {
                    //BarEntryLabels.add("Severely Obese");
                }
                if (Obese_Class_III != 0) {
                    //BarEntryLabels.add("Very Severely Obese");
                    total_count = total_count+1;
                }



                if (Normal != 0){
                    i=i+1;
                    BarEntryLabels.add("Normal");
                    Log.e(TAG,"I0 :"+i);
                    BARENTRY.add(new BarEntry(Normal, i));
                } if (Overweight != 0) {
                    i=i+1;
                    Log.e(TAG,"I1 :"+i);
                    BARENTRY.add(new BarEntry(Overweight, i));
                    BarEntryLabels.add("Overweight");
                } if (Underweight != 0) {
                    i=i+1;
                    Log.e(TAG,"I2 :"+i);

                    BARENTRY.add(new BarEntry(Underweight, i));
                    BarEntryLabels.add("Underweight");
                } if (Severely_Underweight != 0) {
                    i=i+1;
                    Log.e(TAG,"I3 :"+i);

                    BARENTRY.add(new BarEntry(Severely_Underweight, i));
                    BarEntryLabels.add("Severely Underweight");

                } if (Obese_Class_I != 0) {
                    i=i+1;
                    Log.e(TAG,"I4 :"+i);

                    BARENTRY.add(new BarEntry(Obese_Class_I, i));
                    BarEntryLabels.add("Moderately Obese");
                } if (Obese_Class_II != 0) {
                    i=i+1;
                    Log.e(TAG,"I5 :"+i);

                    BARENTRY.add(new BarEntry(Obese_Class_II, i));
                    BarEntryLabels.add("Severely Obese");
                } if (Obese_Class_III != 0) {
                    i=i+1;
                    Log.e(TAG,"I6 :"+i);

                    BARENTRY.add(new BarEntry(Obese_Class_III, i));
                    BarEntryLabels.add("Very Severely Obese");
                }



                Bardataset = new PieDataSet(BARENTRY, "Projects");

                chart.setCenterTextSize(Utils.convertDpToPixel(30));
                BARDATA = new PieData(BarEntryLabels, Bardataset);
                int[] colors = new int[] {Color.parseColor("#4CAF50"), Color.parseColor("#9C27B0"),
                        Color.parseColor("#9E9E9E"),
                        Color.parseColor("#673AB7"),
                        Color.parseColor("#795548"), Color.parseColor("#FFEB3B"), Color.parseColor("#F44336")};
                Bardataset.setColors(colors);
                //Bardataset.setColors(ColorTemplate.COLORFUL_COLORS);

                Bardataset.setValueTextSize(15f);

                chart.getLegend().setEnabled(false);   // Hide the legend
                chart.setData(BARDATA);
                chart.setDescription("");
                chart.setHighlightPerTapEnabled(true);
                chart.setTouchEnabled(true);
//              chart.setGridBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                //Log.e(TAG," OverView All DAta : 2");

                chart.animateY(3000, Easing.EasingOption.EaseOutBack);
                chart.setHighlightPerTapEnabled(true);

                Legend legend = chart.getLegend();
                int colorcodes[] = legend.getColors();

                Log.e(TAG,"Legend Details"+legend.getColors().length);
                Log.e(TAG,"Legend Details"+chart.getXValCount());
                Log.e(TAG,"Legend Details i :"+i);
                Log.e(TAG,"Legend Details j "+j);


                int k = chart.getXValCount()-1;

                if (j==0) {
                    if (j <= k) {
                        Log.e(TAG, "Legend Details1" + legend.getLabel(j));
                        Log.e(TAG, "Legend Details2" + chart.getXValue(j));

                        view_Moderately_Obese1.setText(" " + legend.getLabel(j));

                        view_Moderately_Obese.setBackgroundColor(colorcodes[j]);

                        j++;
                    }
                    if (j <= k) {
                        Log.e(TAG, "Legend Details1" + legend.getLabel(j));
                        Log.e(TAG, "Legend Details2" + chart.getXValue(j));

                        view_Severelyy_Obese1.setText(" " + legend.getLabel(j));
                        view_Severelyy_Obese.setBackgroundColor(colorcodes[j]);


                        j++;
                    }
                    if (j <= k) {
                        Log.e(TAG, "Legend Details" + legend.getLabel(j));
                        view_Severely_Underweight1.setText(" " + legend.getLabel(j));

                        view_Severely_Underweight.setBackgroundColor(colorcodes[j]);

                        j++;
                    }
                    if (j <= k) {
                        Log.e(TAG, "Legend Details" + legend.getLabel(j));
                        view_Severely_Overweight1.setText(" " + legend.getLabel(j));
                        view_Severely_Overweight.setBackgroundColor(colorcodes[j]);

                        j++;
                    }
                    if (j <= k) {
                        Log.e(TAG, "Legend Details" + legend.getLabel(j));
                        view_Overweight1.setText(" " + legend.getLabel(j));
                        view_Overweight.setBackgroundColor(colorcodes[j]);

                        j++;
                    }
                    if (j <= k) {
                        Log.e(TAG, "Legend Details" + legend.getLabel(j));
                        view_normal1.setText(" " + legend.getLabel(j));
                        view_normal.setBackgroundColor(colorcodes[j]);
                        j++;
                    }
                    if (j <= k) {
                        Log.e(TAG, "Legend Details" + legend.getLabel(j));
                        view_Underweight1.setText(" " + legend.getLabel(j));
                        view_Underweight.setBackgroundColor(colorcodes[j]);
                    }
                    j=0;
                }


               // if ()



//                for (j = 0; j <= i ; j++) {
////                    view_normal1.setText(legend.getLabel(j));
////                    view_Moderately_Obese1.setText(legend.getLabel(j));
////                    view_Overweight1.setText(legend.getLabel(j));
////                    view_Underweight1.setText(legend.getLabel(j));
////                    view_Severely_Underweight1.setText(legend.getLabel(j));
////                    view_Severelyy_Obese1.setText(legend.getLabel(j));
////                    view_Severely_Overweight1.setText(legend.getLabel(j));
//                    Log.e(TAG,"Legend Details 1"+chart.getXValCount());
//                    Log.e(TAG,"Legend Details 2"+Bardataset.getColor(i));
//                    Log.e(TAG,"Legend Details 3"+legend.getLabel(j));
//                    Log.e(TAG,"Legend Details 4"+colorcodes[j]);
//
//                }



//                int N=7;
//                 TableRow[] row = new TableRow[N];
//                 TextView[] t = new TextView[N]; // create an empty array;
//
//                for (int i = 0; i < 1; i++) {
//
//                        t[i].setText("text ");
//                        row[i].addView(t[i]);
//
//                    table.addView(row[i], new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//                }




//                for (int i = 0; i <  3; i++) {
//                    Log.e(TAG," Legend Lable:"+legend.getLabel(i)+" Color\n"+colorcodes[i]);
//                    LinearLayout.LayoutParams parms_left_layout = new LinearLayout.LayoutParams(
//                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
//                    parms_left_layout.weight = 1F;
//                    LinearLayout left_layout = new LinearLayout(getActivity());
//                    left_layout.setOrientation(LinearLayout.HORIZONTAL);
//                    left_layout.setGravity(Gravity.LEFT);
//                    left_layout.setLayoutParams(parms_left_layout);
//                    LinearLayout.LayoutParams parms_legen_layout = new LinearLayout.LayoutParams(
//                            20, 20);
//                    parms_legen_layout.setMargins(0, 0, 20, 0);
//                    LinearLayout legend_layout = new LinearLayout(getActivity());
//
//                    legend_layout.setLayoutParams(parms_legen_layout);
//                    legend_layout.setOrientation(LinearLayout.HORIZONTAL);
//                    legend_layout.setBackgroundColor(colorcodes[i]);
//                    left_layout.addView(legend_layout);
//                    TextView txt_unit = new TextView(getActivity());
//                    txt_unit.setText(legend.getLabel(i));
//                    left_layout.addView(txt_unit);
//                    childLayout.addView(left_layout);
//
//                }

                //getTableWithAllRowsStretchedView();

                //Log.e(TAG," OverView All DAta : 3");

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                ee = e;
            }
            @Override
            public void onNothingSelected() {
                Log.e(TAG,"Click 3 "+ chart.getXValue(ee.getXIndex()));

                String value = chart.getXValue(ee.getXIndex());
                editor.putString("Filter_status",value);
                editor.commit();
                RecyclerviewFilterStudentDataShow ldf = new RecyclerviewFilterStudentDataShow();
                Bundle args = new Bundle();
                args.putString("YourKey", value);
                ldf.setArguments(args);
                getFragmentManager().beginTransaction().add(R.id.content_frame, ldf).commit();
                FragmentTransaction tx = getActivity().getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.content_frame, new RecyclerviewFilterStudentDataShow());
                tx.commit();

            }
        });
//        chart.setLongClickable(true);
//        chart.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                Log.e(TAG,"Click 4 ");
//
//                return false;
//            }
//        });


        return root;
    }
    public View getTableWithAllRowsStretchedView() {
        LinearLayout linearLayout = new LinearLayout(getActivity());
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

        TableLayout tableLayout = new TableLayout(getActivity());
        tableLayout.setStretchAllColumns(true);
        tableLayout.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT));
        tableLayout.setWeightSum(4);


        for (int i = 0; i < 4; i++) {
            TableRow tableRow = new TableRow(getActivity());
            tableRow.setGravity(Gravity.CENTER);
            tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT, 1.0f));

            for (int j = 0; j < 4; j++) {
                Button button = new Button(getActivity());
                final int buttonNumber = (j + i * 4);
                button.setText("" + buttonNumber);
                button.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT));

                tableRow.addView(button);
            }
            tableLayout.addView(tableRow);
        }

        linearLayout.addView(tableLayout);
        return linearLayout;
    }

    public void AddValuesToBARENTRY(){



    }

    public void AddValuesToBarEntryLabels(){


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Overview");

    }
}
