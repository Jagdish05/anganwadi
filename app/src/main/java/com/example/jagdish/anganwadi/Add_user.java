package com.example.jagdish.anganwadi;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class Add_user extends Fragment {

    String TAG="Add_user";
    set_user_data set_user_data;
    Boolean male=false,female=false;
    EditText edittext_age,edittext_weight,edittext_height,edittext_hip,edittext_Waist,edittext_id,edittext_name;
    Button submit_user_data;
    Spinner spinner;
    String anganwadi_name="";
    String gender="",name,bmi_status;
    int id;
    //CircularProgressView progressView;

    float age;
    double weight,height,hip,wais;


    CircularProgressView circularProgressView;
    private DatabaseReference mDatabase;
    private String key;
    private FirebaseAuth auth;
    RadioGroup radioGroup;
    ViewGroup root;
    //ProgressDialog progressBar;
    ProgressBar progressBar;
    DatabaseReference mDatabaseReference;
    ArrayList<String> items;
    //ProgressBar progressBar;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup)inflater.inflate(R.layout.activity_add_user,null);

        getActivity().setTitle("Add Student Detail");
        FirebaseCrash.report(new Exception(getActivity().getClass().getSimpleName()));
        FirebaseCrash.log("Activity created");
        auth = FirebaseAuth.getInstance();
        key = auth.getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference(key);
        set_user_data=new set_user_data();
        edittext_id=(EditText)root.findViewById(R.id.edittext_id);
        edittext_name=(EditText)root.findViewById(R.id.edittext_name);
        edittext_age=(EditText)root.findViewById(R.id.edittext_age);
        edittext_weight=(EditText)root.findViewById(R.id.edittext_weight);
        edittext_height=(EditText)root.findViewById(R.id.edittext_height);
//        edittext_hip=(EditText)findViewById(R.id.edittext_hip);
//        edittext_Waist=(EditText)findViewById(R.id.edittext_Waist);
        submit_user_data=(Button)root.findViewById(R.id.submit_user_data);
        spinner = (Spinner)root.findViewById(R.id.spinner);
        radioGroup = (RadioGroup) root.findViewById(R.id.radioGroup);
        //progressBar = (ProgressBar) root.findViewById(R.id.progress_bar);
        //progressView = (CircularProgressView) root.findViewById(R.id.progress_view_add_user);
        // progressView.startAnimation();

        circularProgressView =(CircularProgressView) root.findViewById(R.id.progress_view_add_user);
        circularProgressView.setVisibility(View.INVISIBLE);
        // progressBar = (ProgressBar)root.findViewById(R.id.ventilator_progress);
        // progressBar.setVisibility(View.VISIBLE);
        items = new ArrayList<String>();



//        progressBar = new ProgressDialog(getContext());
//        progressBar.setIndeterminate(true);
//
//        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        progressBar.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        progressBar.show();
        //progressBar = new ProgressBar(getContext(), null, android.R.attr.progressBarStyleSmall);

        //progressBar.setVisibility(View.VISIBLE);

//        ProgressDialog mProgressDialog;
//        mProgressDialog = new ProgressDialog(getActivity());
//        mProgressDialog.setMessage("Signing........");
//        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        mProgressDialog.setCancelable(false);
//

        mDatabaseReference = FirebaseDatabase.getInstance().getReference("Anganwadi_Detail");

        mDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Log.e(TAG,"currunt User 123: "+dataSnapshot.getKey());
                //Log.e(TAG,dataSnapshot.getKey()+" currunt User 12: "+dataSnapshot.getChildrenCount() + "");
                for (DataSnapshot snap: dataSnapshot.getChildren()) {
                    Log.e(TAG,"currunt User 123: "+snap.getKey());
                    //Toast.makeText(getActivity(), " Data" +snap.getKey(), Toast.LENGTH_SHORT).show();
                    items.add(snap.getKey());
                    spinner_load();
                    //Log.e(TAG,"currunt User 123: "+snap + "");
                }
                //progressView.setVisibility(View.GONE);
                //progressView.stopAnimation();
                //notifyAll();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
            submit_user_data.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e(TAG,"Age : "+edittext_age.getText().toString());
                    //Common_Method common_method=new Common_Method();
                    if (isNetworkConnected()) {
                        if (!edittext_age.getText().toString().isEmpty()
                                && !edittext_id.getText().toString().isEmpty()
                                && !edittext_name.getText().toString().isEmpty()
                                && !edittext_weight.getText().toString().isEmpty()
                                && !edittext_height.getText().toString().isEmpty()
                                && !male.toString().isEmpty()
                                && !anganwadi_name.toString().isEmpty()) {
                            Log.e(TAG, "Age : " + edittext_age.getText().toString());

                            try {
                                circularProgressView.setVisibility(View.VISIBLE);
                                circularProgressView.startAnimation();
                                Log.e(TAG, "MAle :" + male + "Female :" + female + "gender :" + gender);
                                age = Float.parseFloat(edittext_age.getText().toString());
                                weight = Double.parseDouble(edittext_weight.getText().toString());
                                height = Double.parseDouble(edittext_height.getText().toString());
//                 hip = Double.parseDouble(edittext_hip.getText().toString());
//                 wais = Double.parseDouble(edittext_Waist.getText().toString());
                                name = edittext_name.getText().toString();
                                id = Integer.parseInt(edittext_id.getText().toString());
                                set_user_data.setTwist(age, weight, height, gender);
                                new data().execute();


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Fill all Text Field", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(getActivity(), "Internet not connected", Toast.LENGTH_SHORT).show();
                    }

                }

            });


//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
//                R.array.planets_array, android.R.layout.simple_spinner_item);
//        spinner.setAdapter(adapter);

        //String[] items = new String[] { "Anganwadi 1", "Anganwadi 2", "Anganwadi 3" ,"Anganwadi 4"};

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId){
                    case R.id.radioGroup_male:
                        Log.e(TAG, "radioGroup_male");
                        gender="m";

                        break;
                    case R.id.radioGroup_female:
                        Log.e(TAG, "radioGroup_female");
                        gender="f";

                        break;

                }
            }
        });
        return root;

    }

//    public void onRadioButtonClicked(View view){
//        Boolean checked = ((RadioButton)view).isChecked();
//    }

    void spinner_load(){
        Log.e(TAG,"list"+items);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, items);
        adapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Log.e(TAG, (String) parent.getItemAtPosition(position));
                anganwadi_name = (String) parent.getItemAtPosition(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
    }
    HttpResponse httpResponse;
    DefaultHttpClient httpClient;
    HttpPost httpPost;
    HttpEntity httpEntity;
    InputStream is=null;
    JSONObject jsonObject;
    public class data extends AsyncTask<String,String,String> {
        @Override
        protected String doInBackground(String... strings) {
            httpClient = new DefaultHttpClient();
            httpPost = new HttpPost("https://bmi.p.mashape.com/");
            httpPost.addHeader("X-Mashape-Key", "97vJCW7a8Imsh1W4v1JmJNEic2b0p19CiSJjsnB06pq3yKRKi4");
            httpPost.addHeader("Content-Type", "application/json");
            httpPost.addHeader("Accept", "application/json");

            JSONObject check = set_user_data.getTwist();
            Log.e(TAG, "check" + check);

            httpPost.setEntity(new ByteArrayEntity(check.toString().getBytes()));
            try {
                httpResponse = httpClient.execute(httpPost);

                BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8"));
                String jsonResponse = reader.readLine();
                jsonObject = new JSONObject(jsonResponse);
                //Log.e(TAG,"Json Object BMI : "+jsonObject.getString("bmi"));
                //Log.e(TAG,"Json Object whr :"+jsonObject.getString("whr"));
                //Log.e(TAG,"Json Object whtr :"+jsonObject.getString("whtr"));


                //is = httpEntity.getContent();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                // Toast.makeText(getActivity(),jsonObject.getString("bmi"),Toast.LENGTH_LONG).show();
                //Toast.makeText(Add_user.this,jsonObject.getString("whr"),Toast.LENGTH_LONG).show();
                //Toast.makeText(Add_user.this,jsonObject.getString("whtr"),Toast.LENGTH_LONG).show();


                String bmi_risk = jsonObject.getJSONObject("bmi").getString("risk");
                bmi_status = jsonObject.getJSONObject("bmi").getString("status");
                //String whr = jsonObject.getJSONObject("whr").getString("status");
                // String whtr = jsonObject.getJSONObject("whtr").getString("status");

                //Log.e(TAG,"bmi_risk :"+bmi_risk+" bmi_status :"+bmi_status+" whr "+whr+" whtr : "+whtr);

                age = Float.parseFloat(edittext_age.getText().toString());
                weight = Double.parseDouble(edittext_weight.getText().toString());
                height = Double.parseDouble(edittext_height.getText().toString());
//                 hip = Double.parseDouble(edittext_hip.getText().toString());
//                 wais = Double.parseDouble(edittext_Waist.getText().toString());
                name = edittext_name.getText().toString();
                id = Integer.parseInt(edittext_id.getText().toString());

                set_student_data set_student_data=new set_student_data(anganwadi_name,id, age, name,weight,height,gender,bmi_status);



                mDatabase.child(String.valueOf(id)).setValue(set_student_data, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if (databaseError != null) {
                            circularProgressView.stopAnimation();
                            circularProgressView.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), databaseError.getMessage(), Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), "Successfull submit Profile", Toast.LENGTH_LONG).show();
                            circularProgressView.stopAnimation();
                            circularProgressView.setVisibility(View.GONE);
                            startActivity(new Intent(getActivity(), Navigation.class));
                        }
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}
