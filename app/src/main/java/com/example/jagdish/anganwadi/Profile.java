package com.example.jagdish.anganwadi;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static android.app.Activity.RESULT_OK;


public class Profile extends Fragment {


    EditText editText_username,editText_name,editText_contact,editText_address,editText_designation;
    Button button_submit,button_take_photo;
    DatabaseReference mDatabase;
    FirebaseAuth auth;
    Boolean empty_data = false;
    String username,name,contact,address,designation,getEmailID;
    String TAG="Profile";
    String image_pic="";
    View root;
    Set_Profile set_profile;
    LinearLayout linearLayout;
    CircularProgressView progressView;

    String username_for_edittext,name_for_edittext,contact_for_edittext,address_for_edittext,designatio_for_edittext;

    //ViewGroup root;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // root = (ViewGroup)inflater.inflate(R.layout.activity_profile,null);
        root = inflater.inflate(R.layout.activity_profile,container,false);
        FirebaseCrash.report(new Exception(getActivity().getClass().getSimpleName()));
        FirebaseCrash.log("Activity created");
        getActivity().setTitle("Edit Profile");

        if(getArguments()!=null) {
            empty_data = getArguments().getBoolean("YourKey");
            Log.e(TAG, " Data  " + empty_data);
        }

//        Bundle bundle = this.getArguments();
//        Log.e(TAG, " Data  " + bundle.getString("YourKey"));


        //Bundle bundle = getActivity().getIntent().getExtras();
        //Log.e(TAG, " Bool " + bundle);

//        if (bundle != null) {
//            empty_data = bundle.getBoolean("Bool");
//            Log.e(TAG, " Bool " + empty_data);
//        }


        camera_permission();
        isStoragePermissionGranted();


        progressView = (CircularProgressView) root.findViewById(R.id.progress_view_profile);
        //progressView.startAnimation();
        progressView.setVisibility(View.INVISIBLE);

        linearLayout = (LinearLayout) root.findViewById(R.id.linearlayout);
        //linearLayout.setVisibility(View.VISIBLE);

        editText_username = (EditText) root.findViewById(R.id.username);
        editText_name = (EditText) root.findViewById(R.id.name);
        editText_contact = (EditText) root.findViewById(R.id.contact);
        editText_address = (EditText) root.findViewById(R.id.address);
        editText_designation = (EditText) root.findViewById(R.id.designation);

        button_submit = (Button) root.findViewById(R.id.submit_profile);
        button_take_photo = (Button) root.findViewById(R.id.take_photo);





        mDatabase = FirebaseDatabase.getInstance().getReference("Profile");

        username = editText_username.getText().toString().trim();
        name = editText_name.getText().toString().trim();
        contact = editText_contact.getText().toString().trim();
        address = editText_address.getText().toString().trim();
        designation = editText_designation.getText().toString().trim();
        //String userId = mDatabase.push().getKey();
        auth = FirebaseAuth.getInstance();
        getEmailID = auth.getCurrentUser().getUid();



        if (!empty_data) {
            Log.e(TAG, " !empty_data " + empty_data);
            submit_profile_data();
        } else
        if (true){
            Log.e(TAG, " empty_data " + empty_data);
            mDatabase.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    if (getEmailID.equals(dataSnapshot.getKey())) {
                        set_profile= dataSnapshot.getValue(Set_Profile.class);
                        Log.e(TAG, " Data get from profile :" + set_profile.contact);

//                        editText_contact.setText("Hello");
//                        editText_username.setText(set_profile.username);
//                        editText_name.setText(set_profile.name);
//                        editText_address.setText(set_profile.address);
//                        editText_designation.setText(set_profile.designation);

                        username_for_edittext = set_profile.username;
                        name_for_edittext = set_profile.name;
                        contact_for_edittext= set_profile.contact;
                        designatio_for_edittext = set_profile.designation;
                        address_for_edittext = set_profile.address;
                        //onResume();
                        //new update().execute();
                        submit_profile_data();
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }

        button_take_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
        return root;
    }

    //    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getActivity().getMenuInflater().inflate(R.menu.navigation, menu);
//        return true;
//    }
    public void submit_profile_data(){
        button_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkConnected()){
                progressView.startAnimation();
                //linearLayout.setVisibility(View.INVISIBLE);
                progressView.setVisibility(View.VISIBLE);
                username = editText_username.getText().toString().trim();
                name = editText_name.getText().toString().trim();
                contact = editText_contact.getText().toString().trim();
                address = editText_address.getText().toString().trim();
                designation = editText_designation.getText().toString().trim();
                Set_Profile set_profile = new Set_Profile(username, name, contact, address, designation,image_pic);
                if (contact.length() == 10 && TextUtils.isDigitsOnly(contact)) {
                    mDatabase.child(getEmailID).setValue(set_profile, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                            //Toast.makeText(Profile.this,""+databaseError,Toast.LENGTH_LONG).show();

                            if (databaseError != null) {
                                Toast.makeText(getActivity().getApplication(), databaseError.getMessage(), Toast.LENGTH_LONG).show();
                                progressView.stopAnimation();
                                progressView.setVisibility(View.GONE);
                            } else {
                                Toast.makeText(getActivity().getApplication(), "Successfull submit", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(getActivity().getApplication(), Navigation.class));
                                progressView.stopAnimation();
                                progressView.setVisibility(View.GONE);
                                //finish();
                            }
                        }
                    });

                } else {
                    Toast.makeText(getActivity().getApplication(), "Enter 10 digit number correct , you enter " + contact.length() + " digit number!", Toast.LENGTH_LONG).show();
                    progressView.stopAnimation();
                    progressView.setVisibility(View.GONE);
                    return;
                }
            } else {
                    Toast.makeText(getActivity().getApplication(),getResources().getString(R.string.internet_error), Toast.LENGTH_LONG).show();

                }
            }
        });
    }

    @Override
    public void onStart(){
        super.onStart();

        Log.e(TAG,"Username show :"+username_for_edittext);
        //editText_contact.setText(username_for_edittext);
//        editText_username.setText(set_profile.username);
//        editText_name.setText(set_profile.name);
//        editText_address.setText(set_profile.address);
//        editText_designation.setText(set_profile.designation);
    }

    @Override
    public void onResume(){
        super.onResume();
        //Log.e(TAG,"Username show :"+username_for_edittext);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout){
            startActivity(new Intent(getActivity().getApplication(), Logout.class));
            // finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    private Bitmap bitmap;
    private File destination = null;
    private InputStream inputStreamImg;
    private String imgPath = null;
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;

    public void selectImage(){
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};
        AlertDialog.Builder altBuilder = new AlertDialog.Builder(getActivity());
        altBuilder.setTitle("Add Photo!");
        altBuilder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (items[which].equals("Take Photo")){
//                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    startActivityForResult(takePicture, 0);
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    //hasPermissionInManifest(getApplicationContext(),Manifest.permission.CAMERA);
                    Log.e(TAG,"Permission taken 1111") ;
                    startActivityForResult(takePictureIntent, 0);



                } else if (items[which].equals("Choose from Library")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto , 1);
                } else if (items[which].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });

        altBuilder.create();
        altBuilder.show();

    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

//        if (Build.VERSION.SDK_INT >= 23) {
//            Log.e(TAG,"Permission taken 201") ;

//            int hasPermission = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
//
//            Log.e(TAG,"Permission taken 202"+hasPermission) ;
//
//            if (hasPermission != PackageManager.PERMISSION_GRANTED) {
//                Log.e(TAG,"Permission taken 203"+hasPermission) ;
//
//                if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//                    Log.e(TAG,"Permission taken 204"+hasPermission) ;
//
//                    // Display UI and wait for user interaction
//                    getErrorDialog("You need to allow Camera permission." +
//                            "\nIf you disable this permission, You will not able to add attachment.", getApplicationContext(), true).show();
//                    Log.e(TAG,"Permission taken 1") ;
//
//                } else {
//                    Log.e(TAG,"Permission taken 22") ;
//                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
//
//                }
//            }
//        }

        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
    public void camera_permission(){
        if (Build.VERSION.SDK_INT >= 23) {
            Log.e(TAG,"Permission taken 201") ;

            int hasPermission = ContextCompat.checkSelfPermission(getActivity().getApplication(), Manifest.permission.CAMERA);

            Log.e(TAG,"Permission taken 202 "+hasPermission) ;

            if (hasPermission != PackageManager.PERMISSION_GRANTED) {
                Log.e(TAG,"Permission taken 203 "+hasPermission) ;

                if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    Log.e(TAG,"Permission taken 204 "+hasPermission) ;

                    // Display UI and wait for user interaction
                    getErrorDialog("You need to allow Camera permission." +
                            "\nIf you disable this permission, You will not able to add attachment.", getActivity().getApplicationContext(), true).show();
                    Log.e(TAG,"Permission taken 1") ;

                } else {
                    Log.e(TAG,"Permission taken 22") ;
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, 0);
                }
                return;
            }
        }
    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                return true;
            } else {

                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
            Log.e(TAG,"Permission: "+permissions[0]+ "was "+grantResults[0]);
            //resume tasks needing this permission
        }
    }
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case 0:
                if(resultCode == RESULT_OK){
                    //Uri picUri = imageReturnedIntent.getData();
                    //Log.e(TAG,"Image Capture  URI "+picUri);

                    //ImageCropFunction(Uri.parse(String.valueOf(imageReturnedIntent)));
                    //isStoragePermissionGranted();
//                    Bitmap original = BitmapFactory.decodeStream(getAssets().open("1024x768.jpg"));
//                    ByteArrayOutputStream out = new ByteArrayOutputStream();
//                    original.compress(Bitmap.CompressFormat.PNG, 100, out);
//                    Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));

                    Bitmap photo = (Bitmap) imageReturnedIntent.getExtras().get("data");

                    Uri tempUri = getImageUri(getActivity().getApplication(), photo);
                    ImageCropFunction(tempUri);
                    Log.e(TAG,"Image Capture  URI "+tempUri);

                    // CALL THIS METHOD TO GET THE ACTUAL PATH
                    //File finalFile = new File(getRealPathFromURI(tempUri));
                    //Log.e(TAG,"Image Capture file "+finalFile);

                    Log.e(TAG, "Pick from Camera::>>> "+photo);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    photo.compress(Bitmap.CompressFormat.PNG, 100, baos);
                    byte[] bytes = baos.toByteArray();
                    String base64Image = Base64.encodeToString(bytes, Base64.DEFAULT);
                    // we finally have our base64 string version of the image, save it.
                    // mDatabase.child("pic").setValue(base64Image);
                    //Log.e(TAG,"URL base64Image "+base64Image);

                    image_pic = base64Image;
                }

                break;
            case 1:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    ImageCropFunction(selectedImage);
                    Log.e(TAG,"URL  "+selectedImage);
                    Log.e(TAG,"URL  "+selectedImage);
                    try {
                        Bitmap  mBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
                        Log.e(TAG,"URL Bitmap 1 "+mBitmap);
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        byte[] bytes = baos.toByteArray();
                        String base64Image = Base64.encodeToString(bytes, Base64.DEFAULT);
                        // we finally have our base64 string version of the image, save it.
                        // mDatabase.child("pic").setValue(base64Image);
                        //Log.e(TAG,"URL base64Image "+base64Image);

                        image_pic = base64Image;

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                break;
            case 2:
                if(resultCode == RESULT_OK){
                    //Uri selectedImage = imageReturnedIntent.getData();
                    //Log.e(TAG,"URL  "+selectedImage);
                    //Log.e(TAG,"URL  "+selectedImage);
                    Bundle extras = imageReturnedIntent.getExtras();
                    Bitmap mBitmap = (Bitmap) extras.get("data");
                    Log.e(TAG,"URL Bitmap 1 "+mBitmap);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] bytes = baos.toByteArray();
                    String base64Image = Base64.encodeToString(bytes, Base64.DEFAULT);
                    // we finally have our base64 string version of the image, save it.
                    // mDatabase.child("pic").setValue(base64Image);
                    //Log.e(TAG,"URL base64Image "+base64Image);

                    image_pic = base64Image;

                }
                break;
        }
    }

    public void ImageCropFunction(Uri picUri) {
        Intent cropIntent = new Intent("com.android.camera.action.CROP");
        cropIntent.setDataAndType(picUri, "image/*");
        cropIntent.putExtra("crop", "true");
        cropIntent.putExtra("aspectX", 1);
        cropIntent.putExtra("aspectY", 1);
        cropIntent.putExtra("outputX", 100);
        cropIntent.putExtra("outputY", 100);
        cropIntent.putExtra("return-data", true);
        startActivityForResult(cropIntent, 2);
    }



//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        startActivity(new Intent(Profile.this,Navigation.class));
//        finish();
//    }

    //    public boolean hasPermissionInManifest(Context context, String permissionName) {
//        final String packageName = context.getPackageName();
//        try {
//            final PackageInfo packageInfo = context.getPackageManager()
//                    .getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
//            final String[] declaredPermisisons = packageInfo.requestedPermissions;
//            if (declaredPermisisons != null && declaredPermisisons.length > 0) {
//                for (String p : declaredPermisisons) {
//                    if (p.equals(permissionName)) {
//                        return true;
//                    }
//                }
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//
//        }
//        return false;
//    }
    public AlertDialog.Builder getErrorDialog(String message, Context context, final boolean isFromCamera) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(getString(R.string.app_name)).setMessage(message);
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                if (Build.VERSION.SDK_INT >= 23) {
                    if(isFromCamera){
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                0);
                    }else {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                1);
                    }
                }

            }
        });
        return alertDialog;
    }
    //return super.onCreateView(inflater, container, savedInstanceState);


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Log.e(TAG,"Username show 231 :"+username_for_edittext);
        ///editText_username = (EditText)view.findViewById(R.id.username);

    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

//    class update extends AsyncTask<String,String,String>{
//
//        @Override
//        protected String doInBackground(String... params) {
//            Log.e(TAG,"Username show 231er3 :"+username_for_edittext);
//
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            editText_username.setText(set_profile.username);
//            editText_contact.setText(username_for_edittext);
//
//            editText_name.setText(set_profile.name);
//
//            editText_address.setText(set_profile.address);
//
//            editText_designation.setText(set_profile.designation);
//        }
//
//    }

}

