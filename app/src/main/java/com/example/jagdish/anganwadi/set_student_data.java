package com.example.jagdish.anganwadi;

/**
 * Created by jagdish on 16/04/17.
 */

public class set_student_data {
    public int id;
    public String name;
    public float age;
    public Double waist;
    public Double hip;
    public Double weight_value;
    public Double height_value;
    public String sex="m",weight_unit="kg",height_unit="cm";
    public String bmi_status;
    public String anganwadi_name;

    public set_student_data(){

    }
    public set_student_data(String anganwadi_name,int id, float age, String name, double weight, double height,String gender, String bmi_status) {
        this.anganwadi_name=anganwadi_name;
        this.id=id;
        this.name=name;
        this.age = age;
        this.weight_value = weight;
        this.height_value = height;
        this.sex=gender;
        this.bmi_status=bmi_status;
    }


}
