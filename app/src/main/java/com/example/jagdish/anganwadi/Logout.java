package com.example.jagdish.anganwadi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.crash.FirebaseCrash;

public class Logout extends AppCompatActivity {
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout);
        FirebaseCrash.report(new Exception("My first Android non-fatal error"));
        FirebaseCrash.log("Activity created");
        auth = FirebaseAuth.getInstance();
        //Toast.makeText(getApplicationContext(),"Call Logout Activity "+auth.getCurrentUser(), Toast.LENGTH_LONG).show();
       if (auth.getCurrentUser() != null ) {
           //Toast.makeText(getApplicationContext(),"User not null ", Toast.LENGTH_LONG).show();
           auth.signOut();
       }
       // Toast.makeText(getApplicationContext()," User "+ auth.getCurrentUser(), Toast.LENGTH_LONG).show();
        startActivity(new Intent(Logout.this, Login.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(Logout.this, Login.class));
        finish();
    }
}
