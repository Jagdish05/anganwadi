package com.example.jagdish.anganwadi;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by jagdish on 19/05/17.
 */

public class Add_Anganwadi_data extends Fragment {

    ViewGroup root;
    EditText editText_anganwadi_name,editText_anganwadi_id,editText_contact,editText_website,editText_emailAddress,editText_address;
    LinearLayout linearLayout;
    CircularProgressView progressView;
    Button button_submit;

    DatabaseReference mDatabaseReference;
    FirebaseAuth auth;

    String TAG="Add_Anganwadi_data";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        root = (ViewGroup)inflater.inflate(R.layout.add_anganwadi,null);
        getActivity().setTitle("Add Anganwadi Detail");



        FirebaseCrash.report(new Exception(getActivity().getClass().getSimpleName()));
        FirebaseCrash.log("Activity created");

        progressView = (CircularProgressView) root.findViewById(R.id.progress_view_profile);
        //progressView.startAnimation();
        progressView.setVisibility(View.INVISIBLE);

        linearLayout = (LinearLayout) root.findViewById(R.id.linearlayout);
        //linearLayout.setVisibility(View.VISIBLE);
        editText_anganwadi_id = (EditText) root.findViewById(R.id.anganwadi_id);
        editText_anganwadi_name = (EditText) root.findViewById(R.id.anganwadi_name);
        editText_contact = (EditText) root.findViewById(R.id.contact);
        editText_address = (EditText) root.findViewById(R.id.address);
        editText_emailAddress = (EditText) root.findViewById(R.id.emailAddress);
        editText_website = (EditText) root.findViewById(R.id.website);

        button_submit = (Button) root.findViewById(R.id.submit_anganwadi_data);

        mDatabaseReference = FirebaseDatabase.getInstance().getReference("Anganwadi_Detail");




        button_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressView.startAnimation();
                progressView.setVisibility(View.VISIBLE);
                if (isNetworkConnected()) {
                    String anganwadi_id = editText_anganwadi_id.getText().toString().trim();
                    String anganwadi_name = editText_anganwadi_name.getText().toString().trim();
                    String contact = editText_contact.getText().toString().trim();
                    String address = editText_address.getText().toString().trim();
                    String emailAddress = editText_emailAddress.getText().toString().trim();
                    String website = editText_website.getText().toString().trim();



                    if (!anganwadi_id.isEmpty()
                            && !anganwadi_name.isEmpty()
                            && !contact.isEmpty()
                            && !address.isEmpty()
                            && !emailAddress.isEmpty()
                            && !website.isEmpty()) {
                        Set_Anganwadi_Data set_anganwadi_data = new Set_Anganwadi_Data(anganwadi_id, anganwadi_name, contact, address, emailAddress, website);

                        if (android.util.Patterns.PHONE.matcher(contact).matches()) {
                            if (Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches()) {
                                mDatabaseReference.child(anganwadi_name).setValue(set_anganwadi_data, new DatabaseReference.CompletionListener() {
                                    @Override
                                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                        if (databaseError != null) {
                                            progressView.stopAnimation();
                                            progressView.setVisibility(View.GONE);
                                            Toast.makeText(getActivity(), databaseError.getMessage(), Toast.LENGTH_LONG).show();
                                        } else {
                                            Toast.makeText(getActivity(), "Successfull submit Profile", Toast.LENGTH_LONG).show();
                                            startActivity(new Intent(getActivity(), Navigation.class));
                                            getActivity().finish();
                                            progressView.stopAnimation();
                                            progressView.setVisibility(View.GONE);
                                        }
                                    }
                                });

                            } else {
                                Toast.makeText(getActivity().getApplication(), "Enter valid email id ", Toast.LENGTH_LONG).show();
                                progressView.stopAnimation();
                                progressView.setVisibility(View.GONE);
                            }
                        } else {
                            Toast.makeText(getActivity().getApplication(), "Enter valid contact number ", Toast.LENGTH_LONG).show();
                            progressView.stopAnimation();
                            progressView.setVisibility(View.GONE);
                        }
                    } else {
                        Toast.makeText(getActivity(), "Fill all Text Field", Toast.LENGTH_SHORT).show();
                        progressView.stopAnimation();
                        progressView.setVisibility(View.GONE);
                    }
                } else {
                    Toast.makeText(getActivity().getApplication(),getResources().getString(R.string.internet_error), Toast.LENGTH_LONG).show();
                    progressView.stopAnimation();
                    progressView.setVisibility(View.GONE);
                }
            }

        });

        return root;

    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}
