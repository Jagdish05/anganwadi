package com.example.jagdish.anganwadi;

/**
 * Created by jagdish on 17/04/17.
 */

public class DataAdapter {
    private String id;
    private String name;
    private String anganwadi_name;
    private String status;
    private String weight;
    private String height;
    private String age;
    private String gender;

    DataAdapter(String id, String name,String anganwadi_name,String status,String weight,String height,
                String age,String gender) {
        this.id = id;
        this.name = name;
        this.anganwadi_name= anganwadi_name;
        this.status = status;
        this.weight = weight;
        this.age = age;
        this.height = height;
        this.gender = gender;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAnganwadi_name() {
        return anganwadi_name;
    }

    public String getStatus() {
        return status;
    }

    public String getWeight() {
        return weight;
    }

    public String getHeight() {
        return height;
    }

    public String getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    //    public void setmText1(String mText1) {
//        this.mText1 = mText1;
//    }
//
//    public String getmText2() {
//        return mText2;
//    }

//    public void setmText2(String mText2) {
//
//    }
}
