package com.example.jagdish.anganwadi;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class RecyclerviewFilterStudentDataShow extends Fragment implements View.OnClickListener{

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static String LOG_TAG = "CardViewActivity";

    private FirebaseAuth auth;
    private  FirebaseDatabase database;
    private  DatabaseReference myRef_current_user;
    CircularProgressView progressView;

    MyRecyclerViewAdapter myRecyclerViewAdapter;
    String TAG="StudentDataShow";
    int index = 0 ;
    DataAdapter obj;
    ArrayList<DataAdapter> results;

    ViewGroup root;
    ArrayList filteredModelList;
    private Paint p;

    RelativeLayout relativeLayout1;

    SharedPreferences.Editor editor;

    SharedPreferences prefs;
    String empty_data = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.activity_recyclerview_filter_student_data_show, null);
        progressView = (CircularProgressView) root.findViewById(R.id.progress_view_student);
        //setHasOptionsMenu(true);

        //progressView.startAnimation();
        //progressView.setVisibility(View.VISIBLE);

//        Toolbar toolbar = (Toolbar) root.findViewById(R.id.toolbar);
//        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
//        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
//        actionBar.setDisplayHomeAsUpEnabled(false);

        editor =  getActivity().getSharedPreferences("For_hint_swipe",Context.MODE_PRIVATE).edit();

        prefs = getActivity().getSharedPreferences("For_hint_swipe",Context.MODE_PRIVATE);
        String filter_status = prefs.getString("Filter_status",null);
        getActivity().setTitle(filter_status);

        Boolean hint_student = prefs.getBoolean("hint_student",false);

        //Toast.makeText(getActivity(),""+ hint_student, Toast.LENGTH_SHORT).show();
        relativeLayout1 = (RelativeLayout)root.findViewById(R.id.tap_and_drag_left_for_student);

        if (hint_student == true){
            relativeLayout1.setVisibility(View.GONE);
        }

        relativeLayout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                relativeLayout1.setVisibility(View.GONE);
                editor.putBoolean("hint_student",true);
                editor.apply();
            }
        });

        FirebaseCrash.report(new Exception(getActivity().getClass().getSimpleName()));
        FirebaseCrash.log("Activity created");
        p = new Paint();
        auth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        myRef_current_user = database.getReference(auth.getCurrentUser().getUid());
        results = new ArrayList<DataAdapter>();
        myRecyclerViewAdapter = new MyRecyclerViewAdapter(results);
        mRecyclerView = (RecyclerView) root.findViewById(R.id.my_recycler_view);
//        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MyRecyclerViewAdapter(getDataSet());
        mRecyclerView.setAdapter(mAdapter);




        return  root;
    }
    @Override
    public void onResume() {
        super.onResume();
        ((MyRecyclerViewAdapter) mAdapter).setOnItemClickListener(new MyRecyclerViewAdapter
                .MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Log.i(LOG_TAG, " Clicked on Item " + position);
            }
        });
    }
    public ArrayList<DataAdapter> getDataSet() {


        //obj= new DataAdapter("Some Primary Text " + index, "Secondary " + index);
        //results.add(index, obj);

        try {


            myRef_current_user.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    //Log.e(TAG,dataSnapshot.getKey()+" currunt User 12: "+dataSnapshot.getChildrenCount() + "");
                    for (DataSnapshot snap : dataSnapshot.getChildren()) {
                        //Log.e(TAG,"currunt User 123: "+index+"");
                        //Log.e(TAG,"currunt User 123: "+snap + "");
                        obj = new DataAdapter(snap.child("id").getValue().toString(), snap.child("name").getValue().toString(),
                                snap.child("anganwadi_name").getValue().toString(), snap.child("bmi_status").getValue().toString(),
                                snap.child("weight_value").getValue().toString(), snap.child("height_value").getValue().toString(),
                                snap.child("age").getValue().toString(), snap.child("sex").getValue().toString());
                        Log.e(TAG, "Index" + index);

                        results.add(index, obj);
                        index++;

                    }
                    progressView.setVisibility(View.GONE);
                    progressView.stopAnimation();
                    updateData(results);
                    initSwipe();
                    //notifyAll();
                    onQueryTextChange1();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
            Log.e(TAG, "Result 1" + results);
        } catch (IndexOutOfBoundsException e) {
        } catch (RuntimeException e) {

        }
            return results;

    }
    public void updateData(ArrayList<DataAdapter> viewModels) {
        //Log.e(TAG,"Result"+results);
        //mAdapter.notifyDataSetChanged();
        //notifyAll();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


//    @Override
//    public boolean onQueryTextSubmit(String query) {
//        Toast.makeText(getContext(), "Name is 1: "+query, Toast.LENGTH_SHORT).show();
//        return false;
//    }
//
//    @Override
//    public boolean onQueryTextChange(String newText) {
//        //Toast.makeText(getContext().this, "Name is 2 : "+newText, Toast.LENGTH_SHORT).show();
//
//        final List<DataAdapter> filteredModelList = filter(results, newText);
//        if (filteredModelList.size() > 0) {
//            myRecyclerViewAdapter.setFilter(filteredModelList);
//            return true;
//        } else {
//            Toast.makeText(getContext(), "Not Found", Toast.LENGTH_SHORT).show();
//            return false;
//        }
//    }



//    public boolean onQueryTextChange1() {
//        // do your search on change or save the last string or...
//        String filter_status = prefs.getString("Filter_status",null);
//        Log.e(TAG, " Data 4 " +filter_status );
//        final List<DataAdapter> filteredModelList = filter(results,filter_status);
//        if (filteredModelList.size() > 0) {
//            //Toast.makeText(getContext(), "Name is 3: "+filteredModelList, Toast.LENGTH_SHORT).show();
//            myRecyclerViewAdapter.setFilter(filteredModelList);
//            return true;
//        } else {
//            Toast.makeText(getContext(), "Not Found", Toast.LENGTH_SHORT).show();
//
//            return false;
//        }
//    }


    public boolean onQueryTextChange1() {
        // do your search on change or save the last string or...
        String filter_status = prefs.getString("Filter_status",null);
        Log.e(TAG, " Data 4 " +filter_status );
        final List<DataAdapter> filteredModelList = filter(results,filter_status);
        if (filteredModelList.size() > 0) {
            //Toast.makeText(getContext(), "Name is 3: "+filteredModelList, Toast.LENGTH_SHORT).show();
            myRecyclerViewAdapter.setFilter(filteredModelList);
            return true;
        } else {
            //Toast.makeText(getContext(), "Not Found", Toast.LENGTH_SHORT).show();

            return false;
        }
    }
    private List<DataAdapter> filter(List<DataAdapter> models, String query) {
        query = query.toLowerCase();

        filteredModelList = new ArrayList<DataAdapter>();
        for (DataAdapter model : models) {
            final String text1 = model.getStatus().toLowerCase();
            if (text1.contains(query)) {
                filteredModelList.add(model);
            }
        }

        updateData(results);
        myRecyclerViewAdapter = new MyRecyclerViewAdapter(filteredModelList);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(myRecyclerViewAdapter);
        myRecyclerViewAdapter.notifyDataSetChanged();
        return filteredModelList;
    }
    private void initSwipe(){
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();

                if (direction == ItemTouchHelper.LEFT){
                    dialogBox(position);
//                    Toast.makeText(getActivity()," Swipe Left : "+results.get(position).getId(), Toast.LENGTH_SHORT).show();
                    //myRef_current_user.child(results.get(position).getId()).removeValue();
                }
            }
            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                Bitmap icon;
                if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE){
                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;
                    if(dX < 0){
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.delete);
                        p.setColor(Color.parseColor("#D32F2F"));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background, p);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(mRecyclerView);
        //notify();
    }
    @Override
    public void onClick(View v) {
    }
    public void dialogBox(final int position) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("Are you sure you want Delete Student ?");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        myRef_current_user.child(results.get(position).getId()).removeValue();
                        mAdapter.notifyItemRemoved(position);
                        mAdapter.notifyDataSetChanged();
                        results.remove(position);
                        setHasOptionsMenu(false);
                        setHasOptionsMenu(true);
                        updateData(results);
                        myRecyclerViewAdapter.deleteItem(position);
                        myRecyclerViewAdapter.notifyItemRemoved(position);

                    }
                });
        alertDialogBuilder.setNegativeButton("cancel",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        mAdapter.notifyDataSetChanged();
                        onQueryTextChange1();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}







