package com.example.jagdish.anganwadi;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import java.io.File;

public class Loading extends AppCompatActivity {
    String TAG="Loading";
    File localFile = null;
    ImageView imageView;
    private static final int MY_NOTIFICATION_ID=1;
    NotificationManager notificationManager;
    Notification myNotification;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        PackageManager packageManager= getApplicationContext().getPackageManager();
//        try {
//            PackageInfo info = packageManager.getPackageInfo(
//                    getApplicationContext().getPackageName(), 0);
//            String version = info.versionName;
//            Log.e(TAG, "Stutus 1 "+version);
//
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                startActivity(new Intent(getApplication(),Login.class));
                finish();
            }
        }).start();
        //onReceive(getApplicationContext(),"sdfsdf","fdsfsdf");
    }



//    public void onReceive(Context context,String title,String body) {
//
//        Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.co.in"));
//        PendingIntent pendingIntent = PendingIntent.getActivity(
//                context,
//                0,
//                myIntent,
//                PendingIntent.FLAG_CANCEL_CURRENT);
//
//        myNotification = new NotificationCompat.Builder(context)
//                .setContentTitle("Exercise of Notification!")
//                .setContentText("Do Something...")
//                .setTicker("Notification!")
//                .setWhen(System.currentTimeMillis())
//                .setContentIntent(pendingIntent)
//                .setDefaults(Notification.DEFAULT_SOUND)
//                .setAutoCancel(true)
//                .setSmallIcon(R.mipmap.logo)
//                .build();
//
//        notificationManager =
//                (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(MY_NOTIFICATION_ID, myNotification);
//    }
//
//
//    public class InstallAPK extends AsyncTask<String,Void,Void> {
//
//        @Override
//        protected Void doInBackground(String... arg0) {
//
//            String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
//            String fileName = "AppName.apk";
//            destination += fileName;
//            final Uri uri = Uri.parse("file://" + destination);
//
//            //Delete update file if exists
//            File file = new File(destination);
//            Log.e(TAG, "Stutus 2 "+file.exists());
//
//            if (file.exists()) {
//                file.delete();
//                Log.e(TAG, "Stutus 3 Deleted file is "+file.exists());
//
//            }
//            String url = "https://firebasestorage.googleapis.com/v0/b/anganwadi-5267d.appspot.com/o/app.apk?alt=media&token=3680499b-5ece-4614-a77a-fe313f03ed51";
//            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
//            request.setDescription(Loading.this.getString(R.string.notification_description));
//            request.setTitle(Loading.this.getString(R.string.app_name));
//
//            //set destination
//            request.setDestinationUri(uri);
//
//            // get download service and enqueue file
//            final DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
//            final long downloadId = manager.enqueue(request);
//
//            //set BroadcastReceiver to install app when .apk is downloaded
//            BroadcastReceiver onComplete = new BroadcastReceiver() {
//                public void onReceive(Context ctxt, Intent intent) {
//                    Intent install = new Intent(Intent.ACTION_VIEW);
//                    install.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    install.setDataAndType(uri,
//                            "application/vnd.android.package-archive");
//                    startActivity(install);
//                    unregisterReceiver(this);
//                    finish();
//                }
//            };
//            //register receiver for when .apk download is compete
//            registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
//            return null;
//
//        }
//    }

}