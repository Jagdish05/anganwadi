package com.example.jagdish.anganwadi;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

/**
 * Created by jagdish on 08/06/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService{
    private static final String TAG = "FCM Service";
    private static final int MY_NOTIFICATION_ID=1;
    NotificationManager notificationManager;
    String value;
    String Title,Body;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO: Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated.
        Log.e(TAG, "From: " + remoteMessage.getFrom());
        Log.e(TAG, "Notification Message Body 1: " + remoteMessage.getNotification().getTitle());
        Log.e(TAG, "Notification Message Body 2: " + remoteMessage.getNotification().getBody());
        Log.e(TAG, "Notification Message Body 3: " + remoteMessage.getNotification().getLink());

        Title=remoteMessage.getNotification().getTitle();
        Body=remoteMessage.getNotification().getBody();
        String key;
        for (Map.Entry<String, String> entry : remoteMessage.getData().entrySet()) {
             key = entry.getKey();
             value = entry.getValue();
            Log.e(TAG, "key, " + key + " value " + value);
        }



//        Intent intent = new Intent(this, Navigation.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1410,
//                intent, PendingIntent.FLAG_ONE_SHOT);
//        startActivity(intent);
//
//        NotificationCompat.Builder notificationBuilder = new
//                NotificationCompat.Builder(this)
//                .setSmallIcon(R.mipmap.logo)
//                .setContentTitle(Title)
//                .setContentText(Body)
//                .setAutoCancel(true)
//                .setContentIntent(pendingIntent);
//
//        NotificationManager notificationManager =
//                (NotificationManager)
//                        getSystemService(Context.NOTIFICATION_SERVICE);
//
//        notificationManager.notify(1410, notificationBuilder.build());





        //Notification(getApplicationContext(),"fdsf");
        //onReceive(getApplicationContext(),Title,Body,value);
    }
//    public void onReceive(Context context, String title, String body ,String value) {
//
//
//    }



//    public void ShowNotification(Context context){
////        Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(value));
////        PendingIntent pendingIntent = PendingIntent.getActivity(
////                context,
////                0,
////                myIntent,
////                Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
////
////        myNotification = new NotificationCompat.Builder(context)
////                .setContentTitle(Title)
////                .setContentText(Body)
////                .setTicker("Notification!")
////                .setWhen(System.currentTimeMillis())
////                .setContentIntent(pendingIntent)
////                .setDefaults(Notification.DEFAULT_SOUND)
////                .setAutoCancel(true)
////                .setSmallIcon(R.mipmap.logo)
////                .build();
////        notificationManager =
////                (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
////        notificationManager.notify(MY_NOTIFICATION_ID, myNotification);
//    }
    public class ShowNotification extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
        Notification(getApplicationContext(),"dfsfsdf");
    }
}


    public void Notification(Context context, String message) {

        // Open NotificationView Class on Notification Click
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(value));
        // Send data to NotificationView Class
        //intent.putExtra("title",Title);
        //intent.putExtra("text", Body);
        // Open NotificationView.java Activity
        PendingIntent pIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        // Create Notification using NotificationCompat.Builder
        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                context)
                // Set Icon
                .setWhen(0)
                .setPriority(Notification.PRIORITY_MAX)
                .setSmallIcon(R.mipmap.logo)
                // Set Ticker Message
                .setTicker(message)
                // Set Title
                .setContentTitle(Title)
                // Set Text
                .setContentText(Body)
                // Add an Action Button below Notification
                .addAction(R.mipmap.logo, "Action Button", pIntent)
                // Set PendingIntent into Notification
                .setContentIntent(pIntent)
                // Dismiss Notification
                .setAutoCancel(true);

        // Create Notification Manager
        NotificationManager notificationmanager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager
        notificationmanager.notify(0, builder.build());

    }
}
