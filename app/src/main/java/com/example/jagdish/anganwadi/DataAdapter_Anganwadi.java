package com.example.jagdish.anganwadi;

/**
 * Created by jagdish on 19/05/17.
 */

public class DataAdapter_Anganwadi {
    public String id;
    public String name;
    public String contact;
    public String address;
    public String website;
    public String email_id;


    public DataAdapter_Anganwadi(String id,String name,String contact,String address,String website,String email_id) {
        this.id=id;
        this.name=name;
        this.address = address;
        this.contact = contact;
        this.website = website;
        this.email_id=email_id;
    }

//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public void setContact(String contact) {
//        this.contact = contact;
//    }
//
//    public void setAddress(String address) {
//        this.address = address;
//    }
//
//    public void setWebsite(String website) {
//        this.website = website;
//    }
//
//    public void setEmail_id(String email_id) {
//        this.email_id = email_id;
//    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getContact() {
        return contact;
    }

    public String getAddress() {
        return address;
    }

    public String getWebsite() {
        return website;
    }

    public String getEmail_id() {
        return email_id;
    }
}
