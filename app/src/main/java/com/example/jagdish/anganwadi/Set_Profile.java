package com.example.jagdish.anganwadi;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by jagdish on 16/03/17.
 */
@IgnoreExtraProperties
public class Set_Profile {

    public String username;
    public String name;
    public String contact;
    public String address;
    public String designation;
    public String image_pic;

    public Set_Profile(){

    }
    public Set_Profile(String username, String name, String contact, String address, String designation,String image_pic){

        this.username = username;
        this.name = name;
        this.contact = contact;
        this.address = address;
        this.designation = designation;
        this.image_pic=image_pic;
    }

}
