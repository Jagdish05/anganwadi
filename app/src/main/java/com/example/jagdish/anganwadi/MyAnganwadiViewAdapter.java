package com.example.jagdish.anganwadi;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by jagdish on 19/05/17.
 */

public class MyAnganwadiViewAdapter extends RecyclerView.Adapter<MyAnganwadiViewAdapter.DataObjectHolder1> {

    static String TAG = "MyAnganwadiViewAdapter";
    private static MyClickListener myClickListener;
    private ArrayList<DataAdapter_Anganwadi> mDataset;

    public static class DataObjectHolder1 extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView anganwadi_id;
        TextView anganwadi_name;
        TextView contact;
        TextView website;
        TextView emailAddress;
        TextView address;

        ImageView l1;
        ImageView l2;
        LinearLayout ll;

        public DataObjectHolder1(View itemView) {
            super(itemView);
            anganwadi_id = (TextView) itemView.findViewById(R.id.text_anganwadi_id);
            anganwadi_name = (TextView) itemView.findViewById(R.id.text_anganwadi_name1);
            contact = (TextView) itemView.findViewById(R.id.text_contact_name_anganwadi);
            website = (TextView) itemView.findViewById(R.id.text_website_anganwadi);
            emailAddress = (TextView) itemView.findViewById(R.id.text_email_id_anganwadi);
            address = (TextView) itemView.findViewById(R.id.text_address_anganwadi);

            l1 = (ImageView) itemView.findViewById(R.id.down_arrow_relative_anganwadi);
            l2 = (ImageView) itemView.findViewById(R.id.up_arrow_relative_anganwadi);
            ll = (LinearLayout) itemView.findViewById(R.id.linearlayout);
            l2.setVisibility(View.GONE);
            ll.setVisibility(View.GONE);
//            l2.setVisibility(View.GONE);

            if (l1.getVisibility() == View.VISIBLE) {
                //Log.e(TAG,"l1 Visible to Gone 1 "+l1.getVisibility());
                l1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        l1.setVisibility(View.GONE);
                        ll.setVisibility(View.VISIBLE);
                        l2.setVisibility(View.VISIBLE);
                    }
                });
            }else{
                //Log.e(TAG,"l1 Visible to Gone 1 "+l1.getVisibility());
            }
            if (l2.getVisibility() == View.GONE) {
                //Log.e(TAG,"l2 Visible to Gone 1 "+l2.getVisibility());
                l2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        l2.setVisibility(View.GONE);
                        ll.setVisibility(View.GONE);
                        l1.setVisibility(View.VISIBLE);
                    }
                });
            }else{
                //Log.e(TAG,"l2 Gone to Visible 2 "+l2.getVisibility());
            }
        }

        @Override
        public void onClick(View v) {

            myClickListener.onItemClick(getAdapterPosition(), v);

            Log.i(TAG, "Adding Listener"+v.getId());


        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
        Log.e(TAG, "Adding Listener");
    }

    public MyAnganwadiViewAdapter(ArrayList<DataAdapter_Anganwadi> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public DataObjectHolder1 onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view_anganwadi, parent, false);

        DataObjectHolder1 dataObjectHolder = new DataObjectHolder1(view);
        return dataObjectHolder;
    }




    @Override
    public void onBindViewHolder(DataObjectHolder1 holder, final int position) {

        //Log.e(TAG, " holder " + holder + " "+position);
        holder.anganwadi_name.setText(mDataset.get(position).getName());
        holder.anganwadi_id.setText(mDataset.get(position).getId());
        //Log.e(TAG, " holder " + holder + " "+position);

        holder.contact.setText(mDataset.get(position).getContact());
        holder.address.setText(mDataset.get(position).getAddress());

        holder.website.setText(mDataset.get(position).getWebsite());
        holder.emailAddress.setText(mDataset.get(position).getEmail_id());


        holder.anganwadi_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e(TAG, " position " +mDataset.get(position).getId());

            }
        });

    }

    public void addItem(DataAdapter_Anganwadi dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        //Log.e(TAG, " Dataset " + mDataset.size());
        return mDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }

    public void setFilter(List<DataAdapter_Anganwadi> countryModels) {
        mDataset = new ArrayList<>();
        mDataset.addAll(countryModels);
        notifyDataSetChanged();
    }
}