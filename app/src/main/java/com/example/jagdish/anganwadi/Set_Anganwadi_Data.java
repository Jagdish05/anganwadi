package com.example.jagdish.anganwadi;

/**
 * Created by jagdish on 19/05/17.
 */

public class Set_Anganwadi_Data {
    public String id;
    public String name;
    public String contact;
    public String address;
    public String website;
    public String email_id;

    public Set_Anganwadi_Data(){

    }
    public Set_Anganwadi_Data(String id,String name,String contact,String address,String website,String email_id) {
        this.id=id;
        this.name=name;
        this.address = address;
        this.contact = contact;
        this.website = website;
        this.email_id=email_id;
    }
}
