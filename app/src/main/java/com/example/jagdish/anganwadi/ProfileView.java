package com.example.jagdish.anganwadi;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class ProfileView extends Fragment {

    private DatabaseReference mDatabase;
    private FirebaseAuth auth;
    String userId;
    TextView textView_username, textView_name, textView_contact, textView_address, textView_designation;
    private Button button_profile_edit;
    private ImageView profile_image;
    String TAG = "Navigation";
    View root;
    //Navigation navigation;
    CircularProgressView progressView;

    LinearLayout linearLayout;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        root = inflater.inflate(R.layout.activity_profile_view,container,false);
        getActivity().setTitle("Profile");

        FirebaseCrash.report(new Exception(getActivity().getClass().getSimpleName()));
        FirebaseCrash.log("Activity created");
        //return inflater.inflate(R.layout.activity_profile_view, container, false);

        profile_image = (ImageView) root.findViewById(R.id.profile_image);
        textView_username = (TextView) root.findViewById(R.id.text_username);
        textView_name = (TextView) root.findViewById(R.id.text_name);
        textView_contact = (TextView) root.findViewById(R.id.text_contact);
        textView_address = (TextView) root.findViewById(R.id.text_address);
        textView_designation = (TextView) root.findViewById(R.id.text_designation);
        button_profile_edit = (Button) root.findViewById(R.id.edit_profile);

        linearLayout =(LinearLayout)root.findViewById(R.id.linearlayout);
        linearLayout.setVisibility(View.INVISIBLE);

        progressView = (CircularProgressView) root.findViewById(R.id.progress_view_profile_view);
        //progressView.startAnimation();
        //progressView.setVisibility(View.VISIBLE);

        //navigation=new Navigation();
        //navigation.progressView.startAnimation();
        //navigation.progressView.setVisibility(View.VISIBLE);

        mDatabase = FirebaseDatabase.getInstance().getReference("Profile");
        auth = FirebaseAuth.getInstance();
        userId = auth.getCurrentUser().getUid();
        //Toast.makeText(ProfileView.this,"UserID :"+userId,Toast.LENGTH_LONG).show();
        //Log.e(TAG,"Current ID :"+auth.getCurrentUser().getUid());

        Query query = mDatabase.child(userId);
        Log.e(TAG,"Query:");
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e(TAG,"Query: 1 "+dataSnapshot);
                if (dataSnapshot.exists()) {
                    Log.e(TAG,"Query: 2 "+dataSnapshot);
                    //startActivity(new Intent(Navigation.this,ProfileView.class));
                    //finish();
                    //fragment = new ProfileView();
                    Set_Profile set_profile = dataSnapshot.getValue(Set_Profile.class);
                    String username = set_profile.username;
                    String name = set_profile.name;
                    String contact = set_profile.contact;
                    String address = set_profile.address;
                    String designation = set_profile.designation;
                    String image_pic = set_profile.image_pic;

                    //Toast.makeText(ProfileView.this,username+" "+name+" "+contact+" "+address+" "+designation,Toast.LENGTH_LONG).show();

                    //Log.e(TAG," ProfileView Url :"+image_pic);

                    textView_username.setText(username);
                    textView_name.setText(name);
                    textView_contact.setText(contact);
                    textView_address.setText(address);
                    textView_designation.setText(designation);
                    byte[] imageAsBytes = Base64.decode(image_pic.getBytes(), Base64.DEFAULT);
                    profile_image.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
//                    navigation.progressView.startAnimation();
//                    navigation.progressView.setVisibility(View.GONE);
                    progressView.stopAnimation();
                    progressView.setVisibility(View.GONE);
                    linearLayout.setVisibility(View.VISIBLE);

                }else {
                    Log.e(TAG,"Query: 3 "+dataSnapshot);
                    progressView.stopAnimation();
                    progressView.setVisibility(View.GONE);

                    //fragment = new Profile();

                    //startActivity(new Intent(Navigation.this,Profile.class));
                    //finish();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


//        mDatabase.addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                //Toast.makeText(ProfileView.this,"KEY :"+dataSnapshot.getKey(),Toast.LENGTH_LONG).show();
//                Log.e(TAG,"Current ID 6:"+dataSnapshot);
//
//                try {
//
//                    if (userId.equals(dataSnapshot.getKey())) {
//                        //Log.e(TAG,"Current ID 7:"+dataSnapshot.getKey());
//                        Set_Profile set_profile = dataSnapshot.getValue(Set_Profile.class);
//                        String username = set_profile.username;
//                        String name = set_profile.name;
//                        String contact = set_profile.contact;
//                        String address = set_profile.address;
//                        String designation = set_profile.designation;
//                        String image_pic = set_profile.image_pic;
//
//                        //Toast.makeText(ProfileView.this,username+" "+name+" "+contact+" "+address+" "+designation,Toast.LENGTH_LONG).show();
//
//                        //Log.e(TAG," ProfileView Url :"+image_pic);
//
//                        textView_username.setText(username);
//                        textView_name.setText(name);
//                        textView_contact.setText(contact);
//                        textView_address.setText(address);
//                        textView_designation.setText(designation);
//                        byte[] imageAsBytes = Base64.decode(image_pic.getBytes(), Base64.DEFAULT);
//                        profile_image.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
////                    navigation.progressView.startAnimation();
////                    navigation.progressView.setVisibility(View.GONE);
//                        progressView.startAnimation();
//                        progressView.setVisibility(View.GONE);
//                        linearLayout.setVisibility(View.VISIBLE);
//
//                    }
//                } catch (NullPointerException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onChildRemoved(DataSnapshot dataSnapshot) {
//
//            }
//
//            @Override
//            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
        button_profile_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(getActivity().getApplicationContext(), Profile.class);
                //intent.putExtra("Bool", true);
//                startActivity(intent);
                Profile ldf = new Profile ();
                Bundle args = new Bundle();
                args.putBoolean("YourKey", true);
                ldf.setArguments(args);
                getFragmentManager().beginTransaction().add(R.id.content_frame, ldf).commit();
                FragmentTransaction tx = getActivity().getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.content_frame, new Profile());
                tx.commit();
            }
        });
        return root;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            startActivity(new Intent(getActivity().getApplication(), Logout.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
    }
}
