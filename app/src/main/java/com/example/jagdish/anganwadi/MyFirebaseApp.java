package com.example.jagdish.anganwadi;

import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by jagdish on 09/06/17.
 */

public class MyFirebaseApp extends android.app.Application {
    @Override
    public void onCreate() {
        super.onCreate();
    /* Enable disk persistence  */
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}
