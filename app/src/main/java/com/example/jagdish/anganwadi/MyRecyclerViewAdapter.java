/*
 * Copyright (c) 2017. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */

package com.example.jagdish.anganwadi;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import static com.github.mikephil.charting.charts.Chart.LOG_TAG;


public class MyRecyclerViewAdapter extends RecyclerView
        .Adapter<MyRecyclerViewAdapter
        .DataObjectHolder> {

    private static String TAG = "Navigation";
    private ArrayList<DataAdapter> mDataset;
    private static MyClickListener myClickListener;


    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        TextView text_student_id;
        TextView text_name;
        TextView text_anganwadi_name;
        TextView text_bmi_status;
        TextView text_weight_value;
        TextView text_height_value;
        TextView text_age;
        TextView text_gender;

        ImageView l1;
        ImageView l2;
        LinearLayout ll;

        public DataObjectHolder(View itemView) {
            super(itemView);
            text_student_id = (TextView) itemView.findViewById(R.id.text_student_id);
            text_name = (TextView) itemView.findViewById(R.id.text_student_name);
            text_anganwadi_name = (TextView) itemView.findViewById(R.id.text_anganwadi_name_for_student);
            text_bmi_status = (TextView) itemView.findViewById(R.id.text_bmi_status);
            text_weight_value = (TextView) itemView.findViewById(R.id.text_weight_value);
            text_height_value = (TextView) itemView.findViewById(R.id.text_height_value);
            text_age = (TextView) itemView.findViewById(R.id.text_age);
            text_gender = (TextView) itemView.findViewById(R.id.text_gender);

            l1 = (ImageView) itemView.findViewById(R.id.down_arrow_relative);
            l2 = (ImageView) itemView.findViewById(R.id.up_arrow_relative);
            ll = (LinearLayout) itemView.findViewById(R.id.linearlayout);
            l2.setVisibility(View.GONE);
            ll.setVisibility(View.GONE);
//            l2.setVisibility(View.GONE);

            if (l1.getVisibility() == View.VISIBLE) {
                Log.e(TAG,"l1 Visible to Gone 1 "+l1.getVisibility());
                l1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        l1.setVisibility(View.GONE);
                        ll.setVisibility(View.VISIBLE);
                        l2.setVisibility(View.VISIBLE);
                    }
                });
            }else{
                Log.e(TAG,"l1 Visible to Gone 1 "+l1.getVisibility());
            }
            if (l2.getVisibility() == View.GONE) {
                Log.e(TAG,"l2 Visible to Gone 1 "+l2.getVisibility());
                l2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        l2.setVisibility(View.GONE);
                        ll.setVisibility(View.GONE);
                        l1.setVisibility(View.VISIBLE);
                    }
                });
            }else{
                Log.e(TAG,"l2 Gone to Visible 2 "+l2.getVisibility());
            }
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
            //v.setVisibility(View.GONE);
            Log.i(LOG_TAG, "Adding Listener");
        }
    }
    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
        Log.i(LOG_TAG, "Adding Listener");
    }
    public MyRecyclerViewAdapter(ArrayList<DataAdapter> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view_row, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }
    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        holder.text_student_id.setText(mDataset.get(position).getId());
        holder.text_name.setText(mDataset.get(position).getName());
        holder.text_anganwadi_name.setText(mDataset.get(position).getAnganwadi_name());
        holder.text_bmi_status.setText(mDataset.get(position).getStatus());
        holder.text_weight_value.setText(mDataset.get(position).getWeight());
        holder.text_height_value.setText(mDataset.get(position).getHeight());
        holder.text_age.setText(mDataset.get(position).getAge());
        holder.text_gender.setText(mDataset.get(position).getGender());

    }

    public void addItem(DataAdapter dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        Log.e(TAG," Dataset "+mDataset.size());
        return mDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }


    public void setFilter(List<DataAdapter> countryModels) {
        mDataset = new ArrayList<>();
        mDataset.addAll(countryModels);
        notifyDataSetChanged();
    }


}