package com.example.jagdish.anganwadi;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.crash.FirebaseCrash;

/**
 * Created by jagdish on 08/06/17.
 */

public class FirebaseCrashNotiFication extends AppCompatActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    public void call_crash_notification(String s){
        FirebaseCrash.report(new Exception(s));
        FirebaseCrash.log("Activity created");
    }
}
