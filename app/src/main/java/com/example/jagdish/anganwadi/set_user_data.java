package com.example.jagdish.anganwadi;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by parth on 24/3/17.
 */
public class set_user_data {

    float age=24;
    Double waist=34.00;
    Double hip=40.00;
    Double weight_value=85.00;
    Double height_value=170.00;
    String sex="m",weight_unit="kg",height_unit="cm";


    public void setTwist(float age,Double weight_value, Double height_value,String sex) {
        this.age = age;
        this.weight_value = weight_value;
        this.height_value = height_value;
        this.waist = waist;
        this.hip = hip;
        this.sex=sex;
    }
    public JSONObject getTwist(){
        JSONObject height=new JSONObject();
        JSONObject weight=new JSONObject();
        JSONObject jobj =new JSONObject();
        try {
            weight.put("value",weight_value);
            weight.put("unit",weight_unit);

            height.put("value",height_value);
            height.put("unit",height_unit);

            jobj.put("height",height);
            jobj.put("weight",weight);

            jobj.put("sex",sex);
            jobj.put("age",age);
//            jobj.put("waist",waist);
//            jobj.put("hip",hip);
            // Log.e(TAG,"Linear data "+linear_data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jobj;
    }

}
