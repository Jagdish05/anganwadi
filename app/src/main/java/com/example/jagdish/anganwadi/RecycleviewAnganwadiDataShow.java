package com.example.jagdish.anganwadi;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jagdish on 19/05/17.
 */
public class RecycleviewAnganwadiDataShow extends Fragment{
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static String LOG_TAG = "CardViewActivity";

    private FirebaseAuth auth;
    private FirebaseDatabase database;
    private DatabaseReference myRef_current_user;
    CircularProgressView progressView;

    MyAnganwadiViewAdapter myRecyclerViewAdapter;

    String TAG="RecycleviewAnganwadiDataShow";

    int index = 0 ;
    DataAdapter_Anganwadi obj;
    ArrayList<DataAdapter_Anganwadi> results;

    ViewGroup root;
    ArrayList filteredModelList;
    private Paint p;

    SharedPreferences.Editor editor;

    SharedPreferences prefs;
    RelativeLayout relativeLayout1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.activity_recycleview_anganwadi_data_show, null);
        progressView = (CircularProgressView) root.findViewById(R.id.progress_view_anganwadi);
        setHasOptionsMenu(true);
        getActivity().setTitle("Anganwadi Details");

        FirebaseCrash.report(new Exception(getActivity().getClass().getSimpleName()));
        FirebaseCrash.log("Activity created");
        //progressView.startAnimation();
        //progressView.setVisibility(View.VISIBLE);

//        Toolbar toolbar = (Toolbar) root.findViewById(R.id.toolbar);
//        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
//        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
//        actionBar.setDisplayHomeAsUpEnabled(false);

        editor =  getActivity().getSharedPreferences("For_hint_swipe",Context.MODE_PRIVATE).edit();

        prefs = getActivity().getSharedPreferences("For_hint_swipe",Context.MODE_PRIVATE);


        Boolean hint_student = prefs.getBoolean("hint_anganwadi",false);

        //Toast.makeText(getActivity(),""+ hint_student, Toast.LENGTH_SHORT).show();
        relativeLayout1 = (RelativeLayout)root.findViewById(R.id.tap_and_drag_left_for_anganwadi);

        if (hint_student == true){
            relativeLayout1.setVisibility(View.GONE);
        }


        relativeLayout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                relativeLayout1.setVisibility(View.GONE);
                editor.putBoolean("hint_anganwadi",true);
                editor.apply();
            }
        });


        p = new Paint();
        auth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        myRef_current_user = database.getReference("Anganwadi_Detail");
        results = new ArrayList<DataAdapter_Anganwadi>();
        myRecyclerViewAdapter = new MyAnganwadiViewAdapter(results);
        mRecyclerView = (RecyclerView) root.findViewById(R.id.my_recycler_view_anganwadi);
//        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MyAnganwadiViewAdapter(getDataSet());
        mRecyclerView.setAdapter(mAdapter);
        return  root;

    }
    @Override
    public void onResume() {
        super.onResume();
        ((MyAnganwadiViewAdapter) mAdapter).setOnItemClickListener(new MyAnganwadiViewAdapter
                .MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                //Log.i(TAG, " Clicked on Item " + position);
            }
        });
    }
    public ArrayList<DataAdapter_Anganwadi> getDataSet() {


        //obj= new DataAdapter("Some Primary Text " + index, "Secondary " + index);
        //results.add(index, obj);
try {
        myRef_current_user.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //Log.e(TAG,dataSnapshot.getKey()+" currunt User 12: "+dataSnapshot.getChildrenCount() + "");
                for (DataSnapshot snap: dataSnapshot.getChildren()) {
//                    Log.e(TAG,"currunt User 123: "+snap);

                    //Log.e(TAG,"currunt User 123: "+index+"");
                    //Log.e(TAG,"currunt User 123: "+snap + "");
                    obj= new DataAdapter_Anganwadi(snap.child("id").getValue().toString(),snap.child("name").getValue().toString(),
                            snap.child("contact").getValue().toString(),snap.child("address").getValue().toString(),
                            snap.child("website").getValue().toString(),snap.child("email_id").getValue().toString());
                    //obj = new DataAdapter_Anganwadi("123321","sfasda","asdasdas","asdadasd","asdadasd","Asdasdad");
                  //  Log.e(TAG,"Result"+obj);

                    results.add(index, obj);
                    initSwipe();
                    index++;
                }
                progressView.setVisibility(View.GONE);
                progressView.stopAnimation();
                updateData(results);
                //notifyAll();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    } catch (IndexOutOfBoundsException e) {
    } catch (RuntimeException e) {

    }
        //Log.e(TAG,"Result"+results);
        return results;
    }
    public void updateData(ArrayList<DataAdapter_Anganwadi> viewModels) {
        //Log.e(TAG,"Result"+results);
        //mAdapter.notifyDataSetChanged();
        //results.addAll(viewModels);
        //notifyAll();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

//    @Override
//    public void onPrepareOptionsMenu(Menu menu) {
//        MenuItem mSearchMenuItem = menu.findItem(R.id.search);
//        SearchView searchView = (SearchView) mSearchMenuItem.getActionView();
//        searchView.setOnQueryTextListener(this);
//
//    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//       //inflater(R.menu.menu_main, menu);
//        menu.clear();
//        inflater.inflate(R.menu.menu_main, menu);
//
//        MenuItem item = menu.findItem(R.id.search);
//        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
//
//        searchView.setOnQueryTextListener(this);
//
//        //return super.onCreateOptionsMenu(menu);
//    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main, menu);
        try {
            // Associate searchable configuration with the SearchView
            SearchManager searchManager =
                    (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
            SearchView searchView =
                    (SearchView) menu.findItem(R.id.search).getActionView();
            searchView.setSearchableInfo(
                    searchManager.getSearchableInfo(getActivity().getComponentName()));
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    // do your search
                    // Toast.makeText(getContext(), "Name is 1: "+s, Toast.LENGTH_SHORT).show();
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    // do your search on change or save the last string or...
                    final List<DataAdapter_Anganwadi> filteredModelList = filter(results, s);
                    if (filteredModelList.size() > 0) {
                        //Toast.makeText(getContext(), "Name is 3: "+filteredModelList, Toast.LENGTH_SHORT).show();

                        myRecyclerViewAdapter.setFilter(filteredModelList);
                        return true;
                    } else {
                        Toast.makeText(getContext(), "Not Found", Toast.LENGTH_SHORT).show();

                        return false;
                    }
                }
            });


        }catch(Exception e)
        {e.printStackTrace();}
    }


//    @Override
//    public boolean onQueryTextSubmit(String query) {
//        Toast.makeText(getContext(), "Name is 1: "+query, Toast.LENGTH_SHORT).show();
//        return false;
//    }
//
//    @Override
//    public boolean onQueryTextChange(String newText) {
//        //Toast.makeText(getContext().this, "Name is 2 : "+newText, Toast.LENGTH_SHORT).show();
//
//        final List<DataAdapter> filteredModelList = filter(results, newText);
//        if (filteredModelList.size() > 0) {
//            myRecyclerViewAdapter.setFilter(filteredModelList);
//            return true;
//        } else {
//            Toast.makeText(getContext(), "Not Found", Toast.LENGTH_SHORT).show();
//            return false;
//        }
//    }



    private List<DataAdapter_Anganwadi> filter(List<DataAdapter_Anganwadi> models, String query) {
        query = query.toLowerCase();

        filteredModelList = new ArrayList<DataAdapter_Anganwadi>();
        for (DataAdapter_Anganwadi model : models) {
            final String text1 = model.getName().toLowerCase();
            final String text2 = model.getId().toLowerCase();
            String text = text1+text2;
            if (text.contains(query)) {
                filteredModelList.add(model);

            }
        }
        //createdata();
        updateData(results);
        myRecyclerViewAdapter = new MyAnganwadiViewAdapter(filteredModelList);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(myRecyclerViewAdapter);
        myRecyclerViewAdapter.notifyDataSetChanged();
        return filteredModelList;
    }
    private void initSwipe(){
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();

                if (direction == ItemTouchHelper.LEFT){
                    dialogBox(position);
//                    Toast.makeText(getActivity()," Swipe Left : "+results.get(position).getId(), Toast.LENGTH_SHORT).show();
                    //myRef_current_user.child(results.get(position).getId()).removeValue();

                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                Bitmap icon;
                if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE){

                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    if(dX < 0){
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.delete);
                        p.setColor(Color.parseColor("#D32F2F"));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background, p);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(mRecyclerView);
        //notify();
    }

    public void dialogBox(final int position) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("Are you sure you want Delete Anganwadi ?");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        myRef_current_user.child(results.get(position).getName()).removeValue();
                        mAdapter.notifyItemRemoved(position);
                        mAdapter.notifyDataSetChanged();
                        results.remove(position);
                        setHasOptionsMenu(false);
                        setHasOptionsMenu(true);
                        updateData(results);
                    }
                });

        alertDialogBuilder.setNegativeButton("cancel",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        mAdapter.notifyDataSetChanged();
                        setHasOptionsMenu(false);
                        setHasOptionsMenu(true);
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
